FROM fedora:38
MAINTAINER celestian "petr.celestian@gmail.com"

RUN echo "fastestmirror=true" >> /etc/dnf/dnf.conf
RUN dnf update -y
RUN dnf install -y openttd openttd-opengfx; dnf clean all

COPY . /app
WORKDIR /app

# Expose the gameplay port
EXPOSE 3979/tcp
EXPOSE 3979/udp

# Expose the admin port
EXPOSE 3977/tcp


ENTRYPOINT ["./entrypoint.sh"]
